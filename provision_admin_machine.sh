#!/bin/bash
#read -p "Entrez l'ip de la vm1" ipvm1
#read -p "Entrez l'ip de la vm2" ipvm2
# sudo su
ipvm1=x.x.x.x
ipvm2=y.y.y.y
password=pwd    #KRPCasdr4M*!31520
apt update
apt upgrade -y
apt install -y git
apt install -y ansible
apt install -y sshpass
ssh-keygen -t rsa -b 2048 -N "" -C "azureuser" -f /home/azureuser/.ssh/id_rsa
chown azureuser:azureuser /home/azureuser/.ssh/id_rsa.pub
chown azureuser:azureuser /home/azureuser/.ssh/id_rsa
sshpass -p "$password" ssh-copy-id -o StrictHostKeyChecking=accept-new -i ~/.ssh/id_rsa.pub azureuser@$ipvm1
sshpass -p "$password" ssh-copy-id -o StrictHostKeyChecking=accept-new -i ~/.ssh/id_rsa.pub azureuser@$ipvm2
#sshpass -p "$password" ssh-copy-id -i /home/azureuser/.ssh/id_rsa.pub azureuser@$ipvm1
#sshpass -p "$password" ssh-copy-id -i /home/azureuser/.ssh/id_rsa.pub azureuser@$ipvm2
git clone https://gitlab.com/asdram/krpc.git /home/azureuser/krpc
chown azureuser:azureuser /home/azureuser/krpc
sed -i "s/x\.x\.x\.x/$ipvm1/g" /home/azureuser/krpc/ansible/hosts
sed -i "s/y\.y\.y\.y/$ipvm2/g" /home/azureuser/krpc/ansible/hosts
ansible-playbook -i /home/azureuser/krpc/ansible/hosts /home/azureuser/krpc/ansible/playbookwp.yml
