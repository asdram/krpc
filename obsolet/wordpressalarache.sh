#!/bin/bash
sudo apt-get update -qq
sudo apt-get install -yqq curl
sudo apt-get install -yqq apache2
sudo apt-get install -yqq php
sudo apt-get install -yqq php-pdo php-mysql php-zip php-gd php-mbstring php-curl php-xml php-pear php-bcmath
sudo apt-get install -yqq mariadb-server
curl -s -o /tmp/latest.zip https://wordpress.org/latest.zip
user="azureuser"
pass="Passw0rd"
dbname="mydb12.mysql.database.azure.com
"
sudo mysql <<_EOF_
UPDATE mysql.user SET Password=PASSWORD('${pass}') WHERE User='root';
DELETE FROM mysql.user WHERE User='';
DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
DROP DATABASE IF EXISTS test;
DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';
FLUSH PRIVILEGES;
_EOF_
sudo mysql -e "CREATE DATABASE $dbname;"
sudo mysql -e "CREATE USER '$user'@'localhost' IDENTIFIED BY '$pass';"
sudo mysql -e "GRANT ALL PRIVILEGES ON $dbname.* TO '$user'@'localhost';"
sudo mysql -e "FLUSH PRIVILEGES;"
sudo rm /var/www/html/index.html
sudo apt-get update -qq
sudo apt-get install -yqq zip
sudo unzip -qq /tmp/latest.zip -d /var/www/html
sudo mv /var/www/html/wordpress/* /var/www/html/
sudo rm /var/www/html/wordpress/ -Rf
sudo chown -R www-data:www-data /var/www/html/