#!/bin/bash

# Téléchargement et installation des dépendances
sudo apt update
sudo apt install -y apache2 php libapache2-mod-php php-mysql

# Téléchargement et extraction de WordPress
wget https://wordpress.org/latest.tar.gz
tar -xzvf latest.tar.gz

# Déplacement des fichiers WordPress vers le répertoire web
sudo mv wordpress/* /var/www/html/

# Configuration de WordPress avec les informations de la base de données Azure SQL
sudo mv /var/www/html/wp-config-sample.php /var/www/html/wp-config.php
sudo sed -i "s/database_name_here/your_db_name/g" /var/www/html/wp-config.php
sudo sed -i "s/username_here/your_db_user/g" /var/www/html/wp-config.php
sudo sed -i "s/password_here/your_db_password/g" /var/www/html/wp-config.php
sudo sed -i "s/localhost/your_db_host/g" /var/www/html/wp-config.php

# Attribution des bonnes autorisations aux fichiers WordPress
sudo chown -R www-data:www-data /var/www/html/
sudo chmod -R 755 /var/www/html/

# Redémarrage du service Apache pour appliquer les modifications
sudo systemctl restart apache2
