#!/bin/bash

# Définition des variables
rg_name="OCC_ASDRAM_equipe1"
location="northeurope"
admin_username="azureuser"

# Création du groupe de ressources
az group create --name $rg_name --location eastus

# Création du réseau virtuel
az network vnet create --resource-group $rg_name --location eastus --name myVNet --address-prefixes 10.1.0.0/16 --subnet-name myBackendSubnet --subnet-prefixes 10.1.0.0/24

# Création de l'adresse IP publique pour le Load Balancer
az network public-ip create --resource-group $rg_name --name myPublicIPStandard --sku Standard # --zone 1

# Création du Load Balancer
az network lb create --resource-group $rg_name --name myLoadBalancer --sku Standard --public-ip-address myPublicIPStandard --frontend-ip-name myFrontEnd --backend-pool-name myBackEndPool

# Création de la sonde de santé pour le Load Balancer
az network lb probe create --resource-group $rg_name --lb-name myLoadBalancer --name myHealthProbe --protocol tcp --port 80

# Création de la règle pour le Load Balancer
az network lb rule create --resource-group $rg_name --lb-name myLoadBalancer --name myHTTPRule --protocol tcp --frontend-port 80 --backend-port 80 --frontend-ip-name myFrontEnd --backend-pool-name myBackEndPool --probe-name myHealthProbe --disable-outbound-snat true --idle-timeout 15 --enable-tcp-reset true

# Création du groupe de sécurité réseau
az network nsg create --resource-group $rg_name --name myNSG

# Création de la règle pour le groupe de sécurité réseau
az network nsg rule create --resource-group $rg_name --nsg-name myNSG --name myNSGRuleHTTP --protocol '*' --direction inbound --source-address-prefix '*' --source-port-range '*' --destination-address-prefix '*' --destination-port-range 80 --access allow --priority 200

# Création de l'adresse IP publique pour Bastion
az network public-ip create --resource-group $rg_name --name myBastionIP --sku Standard # --zone 1

# Création du sous-réseau pour Azure Bastion
az network vnet subnet create --resource-group $rg_name --name AzureBastionSubnet --vnet-name myVNet --address-prefixes 10.1.1.0/27

# Création de Bastion
az network bastion create --resource-group $rg_name --name myBastionHost --public-ip-address myBastionIP --vnet-name myVNet --location eastus

# Création des interfaces réseau pour les machines virtuelles
for vmnic in myNicVM1 myNicVM2
do
  az network nic create --resource-group $rg_name --name $vmnic --vnet-name myVNet --subnet myBackendSubnet --network-security-group myNSG
done

# Création des machines virtuelles Debian
az vm create --resource-group $rg_name --name myVM1 --nics myNicVM1 --image Debian:debian-12:12-arm64:latest --size Standard_D2s_v3 --admin-username $admin_username --no-wait

az vm create --resource-group $rg_name --name myVM2 --nics myNicVM2 --image Debian:debian-12:12-arm64:latest --size Standard_D2s_v3 --admin-username $admin_username --no-wait

# Assignation des adresses IP publiques aux machines virtuelles via le Load Balancer
for vmnic in myNicVM1 myNicVM2
do
  az network nic ip-config address-pool add --address-pool myBackendPool --ip-config-name ipconfig1 --nic-name $vmnic --resource-group $rg_name --lb-name myLoadBalancer
done

# Configuration des extensions pour les machines virtuelles
for vm in myVM1 myVM2
do
  az vm extension set --publisher Microsoft.Azure.Extensions --name CustomScript --version 2.1 --vm-name $vm --resource-group $rg_name --settings '{"fileUris":["./provision_wordpress.sh"],"commandToExecute":"bash provision_wordpress.sh"}'
done

# Affichage de l'adresse IP publique
az network public-ip
