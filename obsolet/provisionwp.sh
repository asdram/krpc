#!/bin/bash

# Mise à jour des packages
sudo apt-get update

# Installation d'Apache
sudo apt-get install -y apache2

# Installation de PHP et des extensions nécessaires pour WordPress
sudo apt-get install -y php php-pdo php-mysql php-zip php-gd php-mbstring php-curl php-xml php-pear php-bcmath

# Téléchargement de WordPress
cd /tmp
wget https://wordpress.org/latest.tar.gz

# Suppression du fichier d'index HTML par défaut
sudo rm /var/www/html/index.html

# Installation de l'utilitaire zip (si ce n'est pas déjà fait)
sudo apt-get install -y zip

# Extraction des fichiers de WordPress dans le répertoire de l'hôte Web
sudo tar -xzvf latest.tar.gz -C /var/www/html --strip-components=1

# Attribution des permissions appropriées sur le répertoire WordPress
sudo chown -R www-data:www-data /var/www/html/

# Redémarrage du service Apache pour appliquer les changements
sudo systemctl restart apache2
