#!/bin/bash

# Mise à jour des packages
sudo apt update

# Installation d'Apache
sudo apt install -y apache2

# Installation de PHP et des modules PHP nécessaires
sudo apt install -y php libapache2-mod-php php-mysql

# Téléchargement et extraction de WordPress
wget https://wordpress.org/latest.tar.gz
tar -xzvf latest.tar.gz

# Copie des fichiers WordPress dans le répertoire du serveur web
sudo cp -R wordpress/* /var/www/html/

# Configuration de WordPress
sudo mv /var/www/html/wp-config-sample.php /var/www/html/wp-config.php
sudo chown -R www-data:www-data /var/www/html/
sudo chmod -R 755 /var/www/html/

# Configuration des informations de la base de données dans le fichier de configuration WordPress
sudo sed -i "s/database_name_here/$mysql_db_name/g" /var/www/html/wp-config.php
sudo sed -i "s/username_here/$mysql_username/g" /var/www/html/wp-config.php
sudo sed -i "s/password_here/$mysql_password/g" /var/www/html/wp-config.php
sudo sed -i "s/localhost/$mysql_server_name/g" /var/www/html/wp-config.php

# Redémarrage du service Apache pour appliquer les modifications
sudo systemctl restart apache2