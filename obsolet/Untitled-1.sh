#!/bin/bash
sudo apt-get update
sudo apt-get install -y apache2
sudo apt-get install -y php
sudo apt-get install -y php-pdo php-mysql php-zip php-gd php-mbstring php-curl php-xml php-pear php-bcmath
sudo apt-get install -y mariadb-server

cd /tmp
wget https://wordpress.org/latest.zip

user="wp_user"
pass="Passw0rd"
dbname="wordpressdb"

mysql <<_EOF_
UPDATE mysql.user SET Password=PASSWORD('${pass}') WHERE User='root';
DELETE FROM mysql.user WHERE User='';
DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
DROP DATABASE IF EXISTS test;
DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';
FLUSH PRIVILEGES;
_EOF_

mysql -e "CREATE DATABASE $dbname;"
mysql -e "CREATE USER '$user'@'localhost' IDENTIFIED BY '$pass';"
mysql -e "GRANT ALL PRIVILEGES ON $dbname.* TO '$user'@'localhost';"
mysql -e "FLUSH PRIVILEGES;"

sudo rm /var/www/html/index.html
sudo apt-get update -y
sudo apt-get install zip
sudo unzip latest.zip -d /var/www/html
cd /var/www/html
sudo mv wordpress/* /var/www/html/
sudo rm wordpress/ -Rf
sudo chown -R www-data:www-data /var/www/html/


#!/bin/bash

# Mise à jour du système et installation des dépendances
sudo apt update
sudo apt install -y apache2 php libapache2-mod-php php-mysql

# Téléchargement et extraction de WordPress
wget https://wordpress.org/latest.tar.gz
tar -xzf latest.tar.gz

# Déplacement des fichiers WordPress vers le répertoire de l'hôte Web
sudo mv wordpress/* /var/www/html/

# Configuration de WordPress
sudo chown -R www-data:www-data /var/www/html/
sudo chmod -R 755 /var/www/html/
sudo mv /var/www/html/wp-config-sample.php /var/www/html/wp-config.php

# Configuration du fichier de configuration de WordPress
sudo sed -i "s/database_name_here/$mysql_db_name/g" /var/www/html/wp-config.php
sudo sed -i "s/username_here/$mysql_username/g" /var/www/html/wp-config.php
sudo sed -i "s/password_here/$mysql_password/g" /var/www/html/wp-config.php
sudo sed -i "s/localhost/$mysql_server_name/g" /var/www/html/wp-config.php

#!/bin/bash

# Variables de la base de données Azure SQL
dbname="wordpressdb"
user="wp_user"
password="password"
server="dbserverwordpress.database.windows.net"

# Mise à jour du système et installation des dépendances
sudo apt update
sudo apt install -y apache2 php libapache2-mod-php php-mysql

# Téléchargement et extraction de WordPress
wget https://wordpress.org/latest.tar.gz
tar -xzf latest.tar.gz

# Déplacement des fichiers WordPress vers le répertoire de l'hôte Web
sudo mv wordpress/* /var/www/html/

# Configuration de WordPress
sudo chown -R www-data:www-data /var/www/html/
sudo chmod -R 755 /var/www/html/
sudo mv /var/www/html/wp-config-sample.php /var/www/html/wp-config.php

# Configuration du fichier de configuration de WordPress avec les informations de la base de données Azure SQL
sudo sed -i "s/database_name_here/$dbname/g" /var/www/html/wp-config.php
sudo sed -i "s/username_here/$user/g" /var/www/html/wp-config.php
sudo sed -i "s/password_here/$password/g" /var/www/html/wp-config.php
sudo sed -i "s/localhost/$server/g" /var/www/html/wp-config.php


sudo apt-get install -y php8.0-common php-mysql php-zip php-gd php-mbstring php-curl php-xml php-pear php-bcmath


sudo ufw allow from 4.255.96.133 to any port 3306


GRANT ALL PRIVILEGES ON *.* TO 'dbkrpcadmin'@'10.1.0.4' IDENTIFIED BY 'Azertyazerty123-!' WITH GRANT OPTION;
FLUSH PRIVILEGES;

GRANT ALL PRIVILEGES ON ** TO 'dbkrpcadmin'@'10.1.0.4' IDENTIFIED BY 'Azertyazerty123-!' WITH GRANT OPTION;
FLUSH PRIVILEGES;