#!/bin/bash
read -p "Entrez mot de passe admin vms" mdpadmin
rg_name="OCC_ASD_Pierre-Claude_test_infra1"
az config set extension.use_dynamic_install=yes_without_prompt

# Création du groupe de ressources
az group create --name $rg_name --location eastus

# Création du réseau virtuel
az network vnet create --resource-group $rg_name --location eastus --name myVNet --address-prefixes 10.1.0.0/16 --subnet-name myBackendSubnet --subnet-prefixes 10.1.0.0/24

# Création de l'adresse IP publique pour le Load Balancer
az network public-ip create --resource-group $rg_name --name myPublicIPStandard --sku Standard # --zone 1

# Création du Load Balancer
az network lb create --resource-group $rg_name --name myLoadBalancer --sku Standard --public-ip-address myPublicIPStandard --frontend-ip-name myFrontEnd --backend-pool-name myBackEndPool

# Création de la sonde de santé pour le Load Balancer
az network lb probe create --resource-group $rg_name --lb-name myLoadBalancer --name myHealthProbe --protocol tcp --port 80

# Création de la règle pour le Load Balancer
az network lb rule create --resource-group $rg_name --lb-name myLoadBalancer --name myHTTPRule --protocol tcp --frontend-port 80 --backend-port 80 --frontend-ip-name myFrontEnd --backend-pool-name myBackEndPool --probe-name myHealthProbe --disable-outbound-snat true --idle-timeout 15 --enable-tcp-reset true

# Création du groupe de sécurité réseau
az network nsg create --resource-group $rg_name --name myNSG

# Création de la règle pour le groupe de sécurité réseau
az network nsg rule create --resource-group $rg_name --nsg-name myNSG --name myNSGRuleHTTP --protocol '*' --direction inbound --source-address-prefix '*' --source-port-range '*' --destination-address-prefix '*' --destination-port-range 80 --access allow --priority 200

# Création de l'adresse IP publique pour Bastion
az network public-ip create --resource-group $rg_name --name myBastionIP --sku Standard # --zone 1

# Création du sous-réseau pour Azure Bastion
az network vnet subnet create --resource-group $rg_name --name AzureBastionSubnet --vnet-name myVNet --address-prefixes 10.1.1.0/27

# Création de Bastion
az network bastion create --resource-group $rg_name --name myBastionHost --public-ip-address myBastionIP --vnet-name myVNet --location eastus

# Création des interfaces réseau pour les machines virtuelles
for vmnic in myNicVM1 myNicVM2
do
  az network nic create --resource-group $rg_name --name $vmnic --vnet-name myVNet --subnet myBackendSubnet --network-security-group myNSG
done

# Création des base de données
az mysql flexible-server create --resource-group OCC_ASD_Pierre-Claude_test_infra5 --name myDB4 --admin-user azureuser --admin-password Passw0rd --sku-name Standard_D2ds_v4 --tier GeneralPurpose --public-access 0.0.0.0 --high-availability SameZone

# az mysql flexible-server create \
#     --resource-group $rg_name \
#     --name myDB1 --admin-user azureuser \
#     --admin-password $mdpadmin --sku-name Standard_B1ms \
#     --tier Burstable --public-access 0.0.0.0

# az mysql flexible-server create \
#     --resource-group $rg_name \
#     --name myDB2 --admin-user azureuser \
#     --admin-password $mdpadmin --sku-name Standard_B1ms \
#     --tier Burstable --public-access 0.0.0.0
#az mysql flexible-server create --resource-group $rg_name --name myDB2 --admin-user azureuser --admin-password $mdpadmin --sku-name Standard_B1ms --tier Burstable --public-access 0.0.0.0
#mysql -h mydb4.mysql.database.azure.com -u azureuser -p "--ssl-ca=DigiCertGlobalRootCA.crt.pem"

# Création des machines virtuelles
az vm create --resource-group $rg_name --name myVM1 --nics myNicVM1 --image Debian11 --size Standard_D2s_v3 --generate-ssh-key --admin-username azureuser --admin-password $mdpadmin --no-wait # --zone 1 --no-wait
az vm create --resource-group $rg_name --name myVM2 --nics myNicVM2 --image Debian11 --size Standard_D2s_v3 --generate-ssh-key --admin-username azureuser --admin-password $mdpadmin --no-wait # --zone 2 --no-wait

# Assignation des adresses IP publiques aux machines virtuelles via le Load Balancer
for vmnic in myNicVM1 myNicVM2
do
  az network nic ip-config address-pool add --address-pool myBackendPool --ip-config-name ipconfig1 --nic-name $vmnic --resource-group $rg_name --lb-name myLoadBalancer
done

# Création de l'adresse IP publique pour le NAT Gateway
az network public-ip create --resource-group $rg_name --name myNATgatewayIP --sku Standard # --zone 1

# Création du NAT Gateway
az network nat gateway create --resource-group $rg_name --name myNATgateway --public-ip-addresses myNATgatewayIP --idle-timeout 10

# Association du sous-réseau au NAT Gateway
az network vnet subnet update --resource-group $rg_name --vnet-name myVNet --name myBackendSubnet --nat-gateway myNATgateway

# Configuration des extensions pour les machines virtuelles

# Start a CustomScript extension to use a simple bash script to update, download and install WordPress and MySQL
az vm extension set \
    --publisher Microsoft.Azure.Extensions \
    --version 2.0 \
    --name CustomScript \
    --vm-name myVM1 \
    --resource-group $rg_name \
    --settings '{"fileUris":["https://gitlab.com/asdram/krpc/-/blob/main/wordpressalarache.sh"],"commandToExecute":"sh wordpressalarache.sh"}'
#  az vm extension set --publisher Microsoft.Compute --version 1.8 --name CustomScriptExtension --vm-name $vm --resource-group $rg_name --settings '{"commandToExecute":"powershell Add-WindowsFeature Web-Server; powershell Add-Content -Path \"C:\\inetpub\\wwwroot\\Default.htm\" -Value $($env:computername)"}'
az vm extension set \
    --publisher Microsoft.Azure.Extensions \
    --version 2.0 \
    --name CustomScript \
    --vm-name myVM2 \
    --resource-group $rg_name \
    --settings '{"fileUris":["https://gitlab.com/asdram/krpc/-/blob/main/wordpressalarache.sh"],"commandToExecute":"sh wordpressalarache.sh"}'
#az vm extension set --publisher Microsoft.Azure.Extensions --version 2.0 --name CustomScript --vm-name myVM2 --resource-group OCC_ASD_Pierre-Claude_test_infra1 --settings '{"fileUris":["https://gitlab.com/asdram/krpc/-/blob/main/wordpressalarache.sh"],"commandToExecute":"sh wordpressalarache.sh"}'

# Création de la ressource du test de charge
az load create --name myTestChargeRessource --resource-group $rg_name --location eastus
az load show --name myTestChargeRessource --resource-group $rg_name

# Affichage de l'adresse IP publique

az network public-ip show --resource-group $rg_name --name myPublicIPStandard --query ipAddress --output tsv
