#!/bin/bash

# Mise à jour des alternatives PHP
sudo update-alternatives --set php /usr/bin/php7.4
sudo update-alternatives --set phar /usr/bin/phar7.4
sudo update-alternatives --set phar.phar /usr/bin/phar.phar7.4
sudo update-alternatives --set phpize /usr/bin/phpize7.4
sudo update-alternatives --set php-config /usr/bin/php-config7.4

# Installation du package php7.4-dev
sudo apt install php7.4-dev

# Configuration de pecl
sudo pecl config-set php_ini /etc/php/7.4/fpm/php.ini
sudo pecl config-set ext_dir /usr/lib/php/20190902

# Installation des extensions SQL Server via pecl
sudo pecl -d php_suffix=7.4 install sqlsrv
sudo pecl -d php_suffix=7.4 install pdo_sqlsrv

# Création des fichiers de configuration pour les extensions
sudo printf "; priority=20\nextension=sqlsrv.so\n" > /etc/php/7.4/mods-available/sqlsrv.ini
sudo printf "; priority=30\nextension=pdo_sqlsrv.so\n" > /etc/php/7.4/mods-available/pdo_sqlsrv.ini

# Activation des extensions pour PHP FPM, Apache2 et CLI
sudo phpenmod -s fpm -v 7.4 sqlsrv pdo_sqlsrv
sudo phpenmod -s apache2 -v 7.4 sqlsrv pdo_sqlsrv
sudo phpenmod -s cli -v 7.4 sqlsrv pdo_sqlsrv

# Redémarrage des services PHP FPM et Apache2
sudo service php7.4-fpm restart
sudo service apache2 restart
