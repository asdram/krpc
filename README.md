
# Projet Haute disponibilité et Supervision d'un site internet - README 👋

Ce projet vise à déployer une infrastructure WordPress sur plusieurs machines virtuelles, avec une base de données Azure SQL. L'infrastructure comprendra un serveur administratif, deux serveurs web WordPress, un équilibreur de charge Azure et une base de données Azure SQL.

# Scripts de déploiement 🛠 :

deploy_infra.sh : Ce script déploie l'infrastructure complète sur Microsoft Azure. Il crée les machines virtuelles, configure les connexions SSH et provisionne les ressources nécessaires.

provision_admin_machine.sh : Ce script provisionne la machine administrative. Il installe les dépendances, configure les connexions SSH pour les autres machines et prépare l'environnement pour le déploiement de WordPress.

# Playbook Ansible 🚀:

provision_wordpress.yml : Ce playbook Ansible provisionne WordPress sur les deux serveurs web WordPress. Il installe Apache, PHP et les modules nécessaires, configure les fichiers de configuration et télécharge WordPress sur les serveurs.
Base de données Azure SQL :

La base de données utilisée pour ce projet est Azure SQL, une solution de base de données managée offerte par Microsoft Azure. Elle assure la haute disponibilité, la sécurité et la sauvegarde automatique des données.

# Instructions d'utilisation ⚡️:

Exécutez le script deploy_infra.sh pour déployer l'infrastructure sur Azure.
Ensuite, exécutez le script provision_admin_machine.sh pour provisionner la machine administrative et configurer les connexions SSH.
Enfin, utilisez le playbook Ansible provision_wordpress.yml pour provisionner WordPress sur les serveurs web.



# Auteurs :

Ce projet a été réalisé par Pierre-Claud Lalbat et Benchrifa Khalid dans le cadre du projet Haute disponibilité et Supervision d'un site internet réalisé au sein de la formation Administrateur DevOps avec le centre de formation Simplon pendant l'année 2024.

